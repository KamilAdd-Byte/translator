package pl.poznan.put.spio.ws;

import lombok.Data;

@Data
public class Response {
    private String origin;
    private String translated;
    private String key;
}
