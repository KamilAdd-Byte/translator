package pl.poznan.put.spio.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

// deklaracja usługi WS
@WebService(targetNamespace = "http://pl.poznan.put.spio/", name = "gaderypoluki")
public interface GaDeRyPoLuKiWs {

    @WebResult(name = "return", targetNamespace = "")
    @WebMethod(action = "urn:translate")
    Response translate(@WebParam(name = "request", targetNamespace = "") Request request);
}
