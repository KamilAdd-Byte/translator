package pl.poznan.put.spio.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

//prosty model odpowiedzi JSON
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TranslatorResponse {

    private String origin;
    private String translated;
    private String key;

}
