# Atranslator
Atranslator jest implementacją translatora gaderypoluki w Angularze. Zawiera logikę biznesową w formie serwisu oraz prostą warstwę prezentacji, która z tego serwisu korzysta.
## Technologie
* Angular
* TypeScript
* SCSS
* Karma
* Jasmine
* Jest
## Uwagi techniczne
### Preferowane IDE
VisualStudio Code lub IntellJ Ultimate
### Uruchamianie programu
`yarn install`

`yarn start`

Następnie wystarczy wejść na http://localhost:4200/
### Uruchamianie testów
Testy jednostkowe logiki są uruchamiana za pomocą biblioteki jest:

`yarn test`

Testy e2e są uruchamiana protractorem. Należy mieć świadomość że projekt może wymagać dostosowania do konkretnego środowiska developerskiego, posiadanego systemu i zainstalowanych wersji bibliotek.

e2e:

`yarn e2e`
